<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MyID Test Suites</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>380f1218-419f-4c3e-a413-5a0ae7e73d05</testSuiteGuid>
   <testCaseLink>
      <guid>c60be70f-3453-46b0-b82a-cd0420025cba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d68ccc4-c08c-4d20-a1d4-3f1a4c79942c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Log Out Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3eed5231-6af6-4db5-8eb0-bcaacc3c2492</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
